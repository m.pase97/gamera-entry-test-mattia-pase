using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpBehaviourScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            PlayerBehaviour player = other.GetComponent<PlayerBehaviour>();
            player.UpdateUI();
            Destroy(gameObject);
        }
    }
}
