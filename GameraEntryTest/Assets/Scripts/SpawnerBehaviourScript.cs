using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnerBehaviourScript : MonoBehaviour
{
    private NavMeshAgent agent;

    //range used for a random pick of the next point to move on
    private float walkRange = 20f;
    
    //next point to move on
    private Vector3 walkPoint;

    //time between next spawn of a pickup
    [SerializeField]
    private float timeToSpawn;

    [SerializeField]
    private GameObject pickUp;

    private Transform hunter;

    //miscellanous used for internal calculus
    [SerializeField]
    private bool isNear = false;

    [SerializeField]
    private bool isSet = false;
    private bool justSpawned = false;

    public LayerMask whatIsPlayer, whatIsGround;

    //range to consider the player near to the spawner
    [SerializeField]
    private float range;

    void Start()
    {
        hunter = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();

        Patrol();

        timeToSpawn = Random.Range(0f, 10f);
    }

    void Update()
    {
        //check the distance between spawner and player
        isNear = Physics.CheckSphere(transform.position, range, whatIsPlayer);

        if (isNear)
        {
            //player is near to, need to spawn pickups and evade from it
            int amount = Random.Range(1, 10);
            if (!justSpawned)
            {
                for (int i = 0; i < amount; ++i)
                {
                    GameObject go = Instantiate(pickUp) as GameObject;
                    go.transform.position = transform.position + new Vector3(0f, 0.5f, 0f);
                }

                justSpawned = true;
            }
            Escape();
        }
        else
        {
            //player is far away, continue to patrol
            Patrol();
            TrySpawn();
        }
    }

    //method used to spawn pickups every random amount of seconds
    void TrySpawn()
    {
        timeToSpawn -= 1 * Time.deltaTime;
        if(timeToSpawn <= 0)
        {
            GameObject go = Instantiate(pickUp) as GameObject;
            go.transform.position = transform.position + new Vector3(0f, 0.5f, 0f);

            timeToSpawn = Random.Range(0f, 10f);
        }
    }

    //patrol method for the normal funcctioning of this behaviour
    void Patrol()
    {
        justSpawned = false;

        //if no poit has been set search it
        if(!isSet)
        {
            SerachPoint();
        }
        else
        {
            //else move towards it
            agent.speed = 20;
            agent.SetDestination(walkPoint);
            Vector3 distanceToTarget = transform.position - walkPoint;

            //position reached
            if (distanceToTarget.magnitude <= 1f)
            {
                isSet = false;
            }
        }
    }

    //Escape method to evade the player
    void Escape()
    {
        //calculate a new point on the map and move to it, boosting the movement speed
        Vector3 distance = (transform.position - hunter.position).normalized;
        distance.y = 0f;
        walkPoint = distance * 20f;
        agent.SetDestination(walkPoint);
        agent.speed = 60;

        isSet = false;
    }

    //method used to select a random point in the map
    void SerachPoint()
    {
        float randomX = Random.Range(-walkRange, walkRange);
        float randomZ = Random.Range(-walkRange, walkRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
        {
            isSet = true;
        }
    }
}
