using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    //UI element to update when you collect an item
    [SerializeField]
    private Text collectedText;

    //integer used to record the number of items collected
    [SerializeField]
    public int pickUpsCollected = 0;

    [SerializeField]
    private CharacterController controller;

    //movement speed
    [SerializeField]
    private float speed = 5f;

    void Update()
    {

        //calculus for player movements
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if(direction.magnitude >= 0.1f)
        {
            controller.Move(direction * speed * Time.deltaTime);
        }
    }

    public void UpdateUI()
    {
        pickUpsCollected += 1;
        collectedText.text = "PICKUPS: " + pickUpsCollected;
    }
}
